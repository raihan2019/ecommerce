<x-layout.master>
    <x-slot:pageTitle>
        Course Create
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
        Course Create
            
            <a class="btn btn-sm btn-primary" href="{{ route('courses.index') }}">List</a>
        </div>
        <div class="card-body">


       <x-alert.errors />
 
     



 <form action="{{ route('courses.store') }}" method="POST" enctype="multipart/form-data">
              @csrf
               
            <div class="mb-3">

        <x-forms.input name="title" type="text" value="{{old('title')}}"/>
        <x-forms.input name="instructor_name" type="text" value="{{old('instructor_name')}}"/>

        <x-forms.input name="batch_no" type="text" value="{{old('batch_no')}}"/>
        <x-forms.input name="class_start_date" type="date" value="{{old('class_start_date')}}"/>
        <x-forms.input name="class_end_date" type="date" value="{{old('class_end_date')}}"/>
        <x-forms.select />
        <!-- <x-forms.input name="course_type" type="select" value="{{old('course_type')}}"/> -->

        <x-forms.input name="image" type="file" />



  


  <div class="mb-3">






    
  
  <div class="form-check">
<x-forms.checkbox />
</div>

<x-forms.button color="primary" class="mt-2" text="submit"/>                                
                                </form>


            </form>




            
        </div>
    </div>
</x-layout.master>