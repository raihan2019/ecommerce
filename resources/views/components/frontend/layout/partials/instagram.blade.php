<div class="container">
                        <h2 class="title title-center">Instagram</h2>
                        <div class="owl-carousel owl-theme row cols-xl-6 cols-lg-5 cols-md-4 cols-sm-3 cols-2 gutter-no"
                            data-owl-options="{
                            'items': 6,
                            'nav': false,
                            'dots': false,
                            'autoplay': true,
                            'autoplayTimeout': 5000,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '576': {
                                    'items': 3
                                },
                                '768': {
                                    'items': 4
                                },
                                '992': {
                                    'items': 5
                                },
                                '1200': {
                                    'items': 6
                                }
                            }
                        }">
                            <figure class="instagram">
                                <a href="#">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/instagram/1.jpg" alt="Instagram" width="257"
                                        height="257" />
                                </a>
                            </figure>
                            <figure class="instagram">
                                <a href="#">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/instagram/2.jpg" alt="Instagram" width="257"
                                        height="257" />
                                </a>
                            </figure>
                            <figure class="instagram">
                                <a href="#">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/instagram/3.jpg" alt="Instagram" width="257"
                                        height="257" />
                                </a>
                            </figure>
                            <figure class="instagram">
                                <a href="#">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/instagram/4.jpg" alt="Instagram" width="257"
                                        height="257" />
                                </a>
                            </figure>
                            <figure class="instagram">
                                <a href="#">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/instagram/5.jpg" alt="Instagram" width="257"
                                        height="257" />
                                </a>
                            </figure>
                            <figure class="instagram">
                                <a href="#">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/instagram/6.jpg" alt="Instagram" width="257"
                                        height="257" />
                                </a>
                            </figure>
                        </div>
                    </div>