@props(['text'=>'edit'])

<a class="btn btn-sm btn-warning" {{$attributes}}>
    {{$text}}
</a>