<div class="container">
                        <h2 class="title title-center">Trending Now</h2>
                        <div class="owl-carousel owl-theme row owl-nav-full owl-shadow-carousel cols-lg-4 cols-md-3 cols-2"
                            data-owl-options="{
                            'items': 4,
                            'nav': false,
                            'dots': true,
                            'loop': false,
                            'margin': 20,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '768': {
                                    'items': 3
                                },
                                '992': {
                                    'items': 4,
                                    'nav': true,
                                    'dots': false
                                }
                            }
                        }">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="/frontend/singlePage">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/products/6.jpg" alt="product" width="345"
                                            height="255">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="/frontend/singlePage">Modern Football Boots</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="/frontend/singlePage" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/6.jpg" href="#"
                                            style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/2.jpg" href="#"
                                            style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/3.jpg" href="#"
                                            style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="/frontend/singlePage">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/products/7.jpg" alt="product" width="345"
                                            height="255">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="/frontend/singlePage">Strong Training Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="/frontend/singlePage" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/7.jpg" href="#"
                                            style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/2.jpg" href="#"
                                            style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/3.jpg" href="#"
                                            style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="/frontend/singlePage">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/products/8.jpg" alt="product" width="345"
                                            height="255">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="/frontend/singlePage">Fashion NIKE Training Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="/frontend/singlePage" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/8.jpg" href="#"
                                            style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/2.jpg" href="#"
                                            style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/3.jpg" href="#"
                                            style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="/frontend/singlePage">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/products/9.jpg" alt="product" width="345"
                                            height="255">
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                                            data-target="#addCartModal" title="Add to cart"><i
                                                class="d-icon-bag"></i></a>
                                        <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                                                class="d-icon-heart"></i></a>
                                    </div>
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="demo7-shop.html">Shoes</a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="/frontend/singlePage">Daisy Fashion Sonia Shoes</a>
                                    </h3>
                                    <div class="product-price">
                                        <ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
                                    </div>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width:100%"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="/frontend/singlePage" class="rating-reviews">( 6 reviews )</a>
                                    </div>
                                    <div class="product-variations">
                                        <a class="color active" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/9.jpg" href="#"
                                            style="background-color: #8f7a68"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/2.jpg" href="#"
                                            style="background-color: #fee2cf"></a>
                                        <a class="color" data-src="{{asset('ui/frontend')}}/images/demos/demo7/products/3.jpg" href="#"
                                            style="background-color: #9a999d"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>