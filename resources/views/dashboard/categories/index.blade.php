<x-layout.master>
    <x-slot:title>
        Category List
    </x-slot:title>
    <x-slot:pageTitle>
        Categories
    </x-slot:pageTitle>
<div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                               Category List
                               <a class="btn btn-sm btn-primary" href="{{route('categories.create')}}">Add New</a>
                            </div>
                         

                            <div class="card-body">
                           
                           <x-alert.message
                            type="success" 
                            :message="session('message')" />


                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Title</th>
                                            <th>Is Active ?</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>

                                    @foreach($categories as $category)
                                    
                                       <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{$category->title}}</td>
                                            <td>{{$category->is_active ? 'Active' : 'In Active'}}</td>
                                            <td>



                 <x-utilities.link-show href="{{ route('categories.show',
                  ['category'=>$category->id])}}" text="show"/>

                      <x-utilities.link-edit href="{{ route('categories.edit',
                          ['category'=>$category->id])}}" text="edit"/>




                                <form method="post" action="{{ route('categories.destroy',['category'=> $category->id]) }}" style="display:inline" >
                                    @csrf
                                    @method('delete')

                                           <x-forms.button color="danger" onclick="return 
                                    confirm('Are you sure Want to Delete? ')" text="Delete"/>                                
                                </form>

                                            </td>
                                        
                                        </tr>   

                                        @endforeach
                                     
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @push('js')
                        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
                       <script src="{{asset('ui/backend')}}/js/datatables-simple-demo.js"></script>
                        @endpush
</x-layout.master>