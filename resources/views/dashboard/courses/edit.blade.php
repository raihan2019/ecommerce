<x-layout.master>
    <x-slot:pageTitle>
        Courses
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
        Course Edit
            
            <a class="btn btn-sm btn-primary" href="{{route('courses.index')}}">List</a>
        </div>
        <div class="card-body">

<x-alert.errors />

<form action="{{ route('courses.update',['course'=>$course->id]) }}" method="POST" enctype="multipart/form-data">
              @csrf

              @method('patch') 
           
              <x-forms.input name="title" type="text" value="{{old('title',$course->title)}}"/>
              <x-forms.input name="instructor_name" type="text" value="{{old('instructor_name',$course->instructor_name)}}"/>
              <x-forms.input name="batch_no" type="text" value="{{old('batch_no',$course->batch_no)}}"/>
              <x-forms.input name="class_start_date" type="date" value="{{old('class_start_date',$course->class_start_date)}}"/>
              <x-forms.input name="class_end_date" type="text" value="{{old('class_end_date',$course->class_end_date)}}"/>



              <img src="{{ asset('storage/courses/'.$course->image) }}" alt="{{ $course->title }} Image" height="100">
        <x-forms.input name="image" type="file" />
         <x-forms.select />
        <!-- <x-forms.input name="course_type" type="select" value="{{old('course_type',$course->course_type)}}"/> -->



  
   



    
  
  <div class="form-check">
  <input name="is_active" class="form-check-input" type="checkbox" value="1" id="isActiveInput" {{$course->is_active ? 'checked' : ''}}>
  <label class="form-check-label" for="isActiveInput">
Is Active
  </label>
</div>

  <button type="submit" class="btn btn-primary mt-2">Submit</button>
            </form>


            
        </div>
    </div>
</x-layout.master>