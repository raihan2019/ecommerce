<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Http\Requests\TagRequest;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{
    public function index(){
        $tags = Tag::all();
        //dd($sizes);
        return view('dashboard.tags.index' ,[
            'tags' => $tags
        ]);
    }

    public function create(){
        return view('dashboard.tags.create');
    }

    public function store(TagRequest $request){

        // $validator = Validator::make($request->all(), [
        //     'title' => 'required|unique:sizes|max:255|min:1',

        // ]);
 
        // if ($validator->fails()) {
        //     return redirect()->back()
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

    

        

        Tag::create([

            'title' => $request->title

        ]);

        
return redirect()->route('tags.index')->withMessage('Successfully Created!');
    }

    public function show($tagId){

$tag=Tag::findOrFail($tagId);


return view('dashboard.tags.show',[
    'tag' => $tag
]);
//dd($size);
       // dd('showing'.$sizeId);
    }

    public function edit($tagId){

$tag=Tag::findOrFail($tagId);

return view('dashboard.tags.edit',compact('tag'));
    }





    public function update(TagRequest $request,$tagId){

    $tag = Tag::findOrFail($tagId);

$tag->update([
    'title' => $request->title
]);

return redirect()->route('tags.index')->with('message','Successfully updated');


    }



    public function destroy($tagId){
        $tag = Tag::findOrFail($tagId);
       $tag->delete();

       
       //return view('dashboard.categories.destroy');

       return redirect()->route('tags.index')->withMessage('Successfully Deleted!');

    }
}
