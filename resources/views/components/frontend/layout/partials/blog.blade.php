<div class="container">
                        <h2 class="title title-center">From Our Blog</h2>
                        <div class="owl-carousel owl-theme row cols-xl-4 cols-lg-3 cols-sm-2 cols-1" data-owl-options="{
                            'items': 4,
                            'dots': true,
                            'nav': false,
                            'loop': false,
                            'margin': 20,
                            'autoPlay': true,
                            'responsive': {
                                '0': {
                                    'items': 1
                                },
                                '576': {
                                    'items': 2
                                },
                                '992': {
                                    'items': 3
                                },
                                '1200': {
                                    'items': 4,
                                    'dots': false
                                }
                            }
                        }">
                            <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.2s'
                            }">
                                <figure class="post-media">
                                    <a href="post-single.html">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/blog/1.jpg" width="370" height="255" alt="post" />
                                    </a>
                                </figure>
                                <div class="post-details">
                                    <div class="post-meta">
                                        on <a href="#" class="post-date">September 27, 2020</a>
                                        | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                    </div>
                                    <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                            Images</a></h4>
                                    <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                        quissapienfacilisis quis sapien.</p>
                                    <a href="post-single.html"
                                        class="btn btn-link btn-underline btn-primary btn-md">Read
                                        More<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.3s'
                            }">
                                <figure class="post-media">
                                    <a href="post-single.html">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/blog/2.jpg" width="370" height="255" alt="post" />
                                    </a>
                                </figure>
                                <div class="post-details">
                                    <div class="post-meta">
                                        on <a href="#" class="post-date">September 27, 2020</a>
                                        | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                    </div>
                                    <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                            Images</a></h4>
                                    <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                        quissapienfacilisis quis sapien.</p>
                                    <a href="post-single.html"
                                        class="btn btn-link btn-underline btn-primary btn-md">Read
                                        More<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.4s'
                            }">
                                <figure class="post-media">
                                    <a href="post-single.html">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/blog/3.jpg" width="370" height="255" alt="post" />
                                    </a>
                                </figure>
                                <div class="post-details">
                                    <div class="post-meta">
                                        on <a href="#" class="post-date">September 27, 2020</a>
                                        | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                    </div>
                                    <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                            Images</a></h4>
                                    <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                        quissapienfacilisis quis sapien.</p>
                                    <a href="post-single.html"
                                        class="btn btn-link btn-underline btn-primary btn-md">Read
                                        More<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="post overlay-dark overlay-zoom appear-animate" data-animation-options="{
                                'name': 'fadeInRightShorter',
                                'delay': '.5s'
                            }">
                                <figure class="post-media">
                                    <a href="post-single.html">
                                        <img src="{{asset('ui/frontend')}}/images/demos/demo7/blog/4.jpg" width="370" height="255" alt="post" />
                                    </a>
                                </figure>
                                <div class="post-details">
                                    <div class="post-meta">
                                        on <a href="#" class="post-date">September 27, 2020</a>
                                        | <a href="#" class="post-comment"><span>2</span> Comments</a>
                                    </div>
                                    <h4 class="post-title"><a href="post-single.html">Just a cool blog post with
                                            Images</a></h4>
                                    <p class="post-content">Londi m velnond ec tellus mass. facilisis
                                        quissapienfacilisis quis sapien.</p>
                                    <a href="post-single.html"
                                        class="btn btn-link btn-underline btn-primary btn-md">Read
                                        More<i class="d-icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>