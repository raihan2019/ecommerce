<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Http\Requests\CourseRequest;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function index(){
        $courses = Course::all();
        //dd($sizes);
        return view('dashboard.courses.index' ,[
            'courses' => $courses
        ]);
    }

    public function create(){
        return view('dashboard.courses.create');
    }

    public function store(CourseRequest $request){

        if($file = $request->file('image')){
            $fileName=date('dmY').time().'.'.$file->getClientOriginalExtension();
            $file->move(storage_path('app/public/courses'),$fileName);
            
            
          }
          

        // $validator = Validator::make($request->all(), [
        //     'title' => 'required|unique:sizes|max:255|min:1',

        // ]);
 
        // if ($validator->fails()) {
        //     return redirect()->back()
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

    

        

        Course::create([

            'title' => $request->title,
            'description' => $request->description,
            'instructor_name'=>$request->instructor_name,
            'batch_no'=>$request->batch_no,
            'class_start_date'=>$request->class_start_date,
            'class_end_date'=>$request->class_end_date,
            'course_type'=>$request->course_type,
            'is_active'=>$request->is_active,
            'image'=>$fileName ?? null

        ]);

        
return redirect()->route('courses.index')->withMessage('Successfully Created!');
    }

    public function show($courseId){

$course=Course::findOrFail($courseId);


return view('dashboard.courses.show',[
    'course' => $course
]);
//dd($size);
       // dd('showing'.$sizeId);
    }

    public function edit($courseId){

$course=Course::findOrFail($courseId);

return view('dashboard.courses.edit',compact('course'));
    }





    public function update(CourseRequest $request,$courseId){

    $course = Course::findOrFail($courseId);


    if($file = $request->file('image')){
        $fileName=date('dmY').time().'.'.$file->getClientOriginalExtension();
        $file->move(storage_path('app/public/courses'),$fileName);

        
      }

$course->update([
    'title' => $request->title,
    'description' => $request->description,
    'instructor_name'=>$request->instructor_name,
    'batch_no'=>$request->batch_no,
    'class_start_date'=>$request->class_start_date,
    'class_end_date'=>$request->class_end_date,
    'course_type'=>$request->course_type,
    'is_active'=>$request->is_active,
    'image'=>$fileName ?? null

]);

return redirect()->route('courses.index')->with('message','Successfully updated');


    }



    public function destroy($courseId){
        $course = Course::findOrFail($courseId);
       $course->delete();

       
       //return view('dashboard.categories.destroy');

       return redirect()->route('courses.index')->withMessage('Successfully Deleted!');

    }
}
