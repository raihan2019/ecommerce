<x-layout.master>
    <x-slot:title>
        Course Details
    </x-slot:title>
    <x-slot:pageTitle>
        Course Details
    </x-slot:pageTitle>
<div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                               Course Details
                               <a class="btn btn-sm btn-primary" href="{{route('courses.index')}}">List</a>
                            </div>
                         
                          <div class="card-body">



                                <table class="table">
                                    
                                        <tr>
                                            <th>Title</th>
                                            <th>{{$course->title}}</th>
                                           
                                        </tr>
                                        
                                        <tr>
                                            <th>Instructor_name</th>
                                            <th>{{$course->instructor_name}}</th>
                                           
                                        </tr>
                                        
                                        <tr>
                                            <th>Batch_No</th>
                                            <th>{{$course->batch_no}}</th>
                                           
                                        </tr>
                                        
                                        <tr>
                                            <th>Class_Start_Date</th>
                                            <th>{{$course->class_start_date}}</th>
                                           
                                        </tr>
                                        
                                        <tr>
                                            <th>Class_End_Date</th>
                                            <th>{{$course->class_end_date}}</th>
                                           
                                        </tr>

                                        <tr>
                                            <th>Course_type</th>
                                            <th>{{$course->course_type}}</th>
                                           
                                        </tr>

                                        <tr>
                                            <th>Image</th>
                                            <th><img src="{{ asset('storage/courses/'.$course->image) }}" alt="{{ $course->title }} Image" height="100"></th>
                                           
                                        </tr>
                                      
                                        <tr>
                                            <th>Is Active</th>
                                            <th>{{$course->is_active ? 'Active' : 'In Active'}}</th>
                                           
                                        </tr>
      
                                </table>
                            </div>
                        </div>
                        @push('js')
                        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
                       <script src="{{asset('ui/backend')}}/js/datatables-simple-demo.js"></script>
                        @endpush
</x-layout.master>