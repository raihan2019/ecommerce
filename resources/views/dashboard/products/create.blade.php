<x-layout.master>
    <x-slot:pageTitle>
        Add Products
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Product List
            <a class="btn btn-sm btn-primary" href="{{route('products.index')}}">List</a>
        </div>
        <div class="card-body">
            <form>
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-floating mb-3 mb-md-0">
                            <input class="form-control" id="inputFirstName" type="text" placeholder="product title" />
                            <label for="inputFirstName">Id</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating">
                            <input class="form-control" id="inputLastName" type="text" placeholder="product description" />
                            <label for="inputLastName">Title</label>
                        </div>
                    </div>
                </div>
                <div class="form-floating mb-3">
                    <input class="form-control" id="inputEmail" type="text" placeholder="product size" />
                    <label for="inputEmail">Description</label>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-floating mb-3 mb-md-0">
                            <input class="form-control" id="inputPassword" type="text" placeholder="Product Price" />
                            <label for="inputPassword">Price</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                    </div>
                </div>
                <div class="mt-4 mb-0">
                    <div class="d-grid"><a class="btn btn-primary btn-block" href="{{ route('admin.home') }}">Add Product</a></div>
                </div>
            </form>
        </div>
    </div>
</x-layout.master>