<x-layout.master>
    <x-slot:pageTitle>
        District Add
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            District List
            <a class="btn btn-sm btn-primary" href="{{route('districts.index')}}">List</a>
        </div>
        <div class="card-body">

        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <form method="POST" action="{{ route('districts.store') }}">

            @csrf

            <div class="mb-3">
    <label for="titleInput" class="form-label">Product Size</label>
    <input name="title" type="text" class="form-control @error('title') is-invalid @enderror" id="titleInput">
    

    @error('title')
    <div class="form-text text-danger">{{ $message }}</div>

    @enderror
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</x-layout.master>