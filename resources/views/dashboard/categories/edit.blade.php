<x-layout.master>
    <x-slot:pageTitle>
        Categories
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
        Category Edit
            
            <a class="btn btn-sm btn-primary" href="{{route('categories.index')}}">List</a>
        </div>
        <div class="card-body">

<x-alert.errors />

<form action="{{ route('categories.update',['category'=>$category->id]) }}" method="POST" enctype="multipart/form-data">
              @csrf

              @method('patch') 
           
              <x-forms.input name="title" type="text" value="{{old('title',$category->title)}}"/>
              <img src="{{ asset('storage/categories/'.$category->image) }}" alt="{{ $category->title }} Image" height="100">
        <x-forms.input name="image" type="file" />
        <x-forms.textarea name="description" :value="old('description',$category->description)" />



  
   



    
  
  <div class="form-check">
  <input name="is_active" class="form-check-input" type="checkbox" value="1" id="isActiveInput" {{$category->is_active ? 'checked' : ''}}>
  <label class="form-check-label" for="isActiveInput">
Is Active
  </label>
</div>

  <button type="submit" class="btn btn-primary mt-2">Submit</button>
            </form>


            
        </div>
    </div>
</x-layout.master>