<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:categories|max:255|min:3',
            // 'instructor_name' => 'nullable|min:10|max:1000',
            // 'batch_no' => 'nullable|min:1|max:1000',
            // 'course_type' => 'nullable|min:1|max:1000',


        ];
    }
}
