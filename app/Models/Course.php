<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    use HasFactory;

    protected $fillable = ['title' ,'instructor_name', 'batch_no','class_start_date','class_end_date','is_active','image','course_type'];
}
