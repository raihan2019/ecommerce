<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\District;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\DistrictRequest;


class DistrictController extends Controller
{
    public function index(){
        $districts = District::all();
        //dd($sizes);
        return view('dashboard.districts.index' ,[
            'districts' => $districts
        ]);
    }

    public function create(){
        return view('dashboard.districts.create');
    }

    public function store(DistrictRequest $request){

        // $validator = Validator::make($request->all(), [
        //     'title' => 'required|unique:sizes|max:255|min:1',

        // ]);
 
        // if ($validator->fails()) {
        //     return redirect()->back()
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }
        
        District::create([

            'title' => $request->title

        ]);

        
return redirect()->route('districts.index')->withMessage('Successfully Created!');
    }

    public function show($districtId){

$district=District::findOrFail($districtId);


return view('dashboard.districts.show',[
    'district' => $district
]);
//dd($size);
       // dd('showing'.$sizeId);
    }

    public function edit($districtId){

$district=District::findOrFail($districtId);

return view('dashboard.districts.edit',compact('district'));
    }





    public function update(DistrictRequest $request,$districtId){

    $district = District::findOrFail($districtId);

$district->update([
    'title' => $request->title
]);

return redirect()->route('districts.index')->with('message','Successfully updated');


    }



    public function destroy($districtId){
        $district = District::findOrFail($districtId);
       $district->delete();

       
       //return view('dashboard.categories.destroy');

       return redirect()->route('districts.index')->withMessage('Successfully Deleted!');

    }
}
