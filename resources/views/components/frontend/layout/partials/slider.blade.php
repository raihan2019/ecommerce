<section class="intro-section">
                    <div class="owl-carousel owl-theme row owl-nav-arrow owl-nav-fade  intro-slider mb-2 animation-slider cols-1 gutter-no"
                        data-owl-options="{
                        'nav': false,
                        'dots': false,
                        'loop': false,
                        'items': 1,
                        'autoplay': false,
                        'responsive': {
                            '992': {
                                'nav': true
                            }
                        }
                    }">
                        <div class="banner banner-fixed intro-slide1">
                            <figure>
                                <img src="{{asset('ui/frontend')}}/images/demos/demo7/slides/1.jpg" alt="intro-banner" width="1903" height="912"
                                    style="background-color: #f3f3f3" />
                            </figure>
                            <div class="container">
                                <div class="banner-content y-50 d-block d-lg-flex align-items-center">
                                    <div class="banner-content-left slide-animate" data-animation-options="{
                                        'name': 'fadeInLeftShorter', 'duration': '1s'
                                    }">
                                        <h4 class="banner-subtitle text-uppercase text-dark">The Riode Shoes Store</h4>
                                        <h3 class="banner-title text-uppercase text-dark">Spring Crick</h3>
                                    </div>
                                    <div class="banner-content-right ml-lg-auto slide-animate" data-animation-options="{
                                        'name': 'fadeInRightShorter', 'duration': '1s'
                                    }">
                                        <h4 class="banner-subtitle text-primary text-uppercase font-weight-bold">Best
                                            Sellers</h4>
                                        <h3 class="banner-title text-uppercase text-white font-weight-bold mb-6">Chooses
                                            to extra comfort all around</h3>
                                        <a href="{{route('shop')}}" class="btn btn-dark btn-rounded">Shop Now<i
                                                class="d-icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="banner banner-fixed intro-slide2">
                            <figure>
                                <img src="{{asset('ui/frontend')}}/images/demos/demo7/slides/2.jpg" alt="intro-banner" width="1903"
                                    height="912" />
                            </figure>
                            <div class="container">
                                <div class="banner-content y-50 pl-2">
                                    <div class="slide-animate" data-animation-options="{
                                        'name': 'fadeInRightShorter'
                                    }">
                                        <h4 class="banner-subtitle text-uppercase text-primary ls-s">From Online store
                                        </h4>
                                        <h3 class="banner-title text-dark ls-m">Originals Comper Star. Shoes</h3>
                                        <p class="font-weight-semi-bold text-uppercase">f o r - M e n<br><span>Product
                                                identifier: dD1160</span></p>
                                        <a href="demo7-shop.html" class="btn btn-dark btn-rounded">Shop Now<i
                                                class="d-icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category-wrap row cols-lg-4 cols-sm-2 cols-1 gutter-sm">
                        <div class="category category-absolute category-banner appear-animate mb-2"
                            data-animation-options="{
                            'name': 'fadeInLeftShorter',
                            'delay': '.4s'
                        }">
                            <a href="#">
                                <figure class="category-media">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/categories/1.jpg" alt="category" width="468"
                                        height="320" />
                                </figure>
                            </a>
                            <div class="category-content align-items-center x-50 w-100">
                                <h4 class="category-name text-uppercase">All Star Nike Shoes</h4>
                                <span class="category-count text-uppercase">
                                    <span>16</span> Products
                                </span>
                                <a href="demo7-shop.html" class="btn btn-underline btn-link">Shop Now</a>
                            </div>
                        </div>

                        <div class="banner banner-fixed overlay-effect1 appear-animate mb-2" data-animation-options="{
                            'name': 'fadeInLeftShorter',
                            'delay': '.2s'
                        }">
                            <a href="#">
                                <figure class="category-media">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/categories/2.jpg" alt="category" width="468"
                                        height="320" style="background-color: rgb(43, 43, 35);" />
                                </figure>
                            </a>
                            <div class="banner-content text-center x-50 y-50 w-100">
                                <h4 class="banner-subtitle text-uppercase text-primary font-weight-bold">Seasonal</h4>
                                <h3 class="banner-title text-white ls-m">Clearance</h3>
                                <p class="font-weight-semi-bold">Top 5 trends From Riode</p>
                                <a href="demo7-shop.html" class="btn btn-white btn-link btn-underline">Shop Now<i
                                        class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>

                        <div class="category category-absolute category-banner appear-animate mb-2"
                            data-animation-options="{
                            'name': 'fadeInRightShorter',
                            'delay': '.2s'
                        }">
                            <a href="#">
                                <figure class="category-media">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/categories/3.jpg" alt="category" width="468"
                                        height="320" />
                                </figure>
                            </a>
                            <div class="category-content align-items-center x-50 w-100">
                                <h4 class="category-name text-uppercase">All star Nike Shoes</h4>
                                <span class="category-count text-uppercase">
                                    <span>16</span> Products
                                </span>
                                <a href="demo7-shop.html" class="btn btn-underline btn-link x-50">Shop Now</a>
                            </div>
                        </div>

                        <div class="banner banner-fixed overlay-effect1 appear-animate mb-2" data-animation-options="{
                            'name': 'fadeInRightShorter',
                            'delay': '.4s'
                        }">
                            <a href="#">
                                <figure class="category-media">
                                    <img src="{{asset('ui/frontend')}}/images/demos/demo7/categories/4.jpg" alt="category" width="468"
                                        height="320" style="background-color: rgb(196, 140, 92);" />
                                </figure>
                            </a>
                            <div class="banner-content text-center x-50 y-50 w-100">
                                <h4 class="banner-subtitle text-uppercase font-weight-bold">Seasonal</h4>
                                <h3 class="banner-title text-white ls-m">The Latest</h3>
                                <p class="font-weight-semi-bold text-white">Discover the new design</p>
                                <a href="demo7-shop.html" class="btn btn-white btn-link btn-underline">Shop Now<i
                                        class="d-icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </section>