<x-layout.master>
    <x-slot:title>
        Brand List
    </x-slot:title>
    <x-slot:pageTitle>
        Brands
    </x-slot:pageTitle>
<div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                               Brand List
                               <a class="btn btn-sm btn-primary" href="{{route('brands.create')}}">Add New</a>
                            </div>
                          





                            <div class="card-body">
                           

                            @if(request()->session()->has('message'))
                            <div class="alert alert-success" role="alert">
                               Successfully Created
                            </div>

                            @endif


                                <table id="datatablesSimple">
                                    <thead>



                                        <tr>
                                            <th>Sl#</th>
                                            <th>Title</th>

                                            
                                        </tr>



                                    </thead>
                                    
                                    <tbody>
                                        @foreach($brands as $brand)

                                <tr>

                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $brand->title }}</td>

                                    <td>
                                        <a href="{{ route('brands.show',['brand'=>$brand->id]) }}" class="btn btn-sm btn-primary">Show</a>
                                        <a href="{{ route('brands.edit',['brand'=>$brand->id]) }}" class="btn btn-sm btn-info">Edit</a>
                                       


                                <form method="post" action="{{ route('brands.destroy',['brand'=> $brand->id]) }}" style="display:inline" >
                                    @csrf
                                    @method('delete')

                                    <button class="btn btn-sm btn-danger" onclick="return 
                                    confirm('Are you sure Want to Delete?')">
                                    Delete</button>                                
                                </form>




                                    </td>
                                    </tr>


                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @push('js')
                        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
                       <script src="{{asset('ui/backend')}}/js/datatables-simple-demo.js"></script>
                        @endpush
</x-layout.master>