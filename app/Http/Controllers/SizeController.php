<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Size;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\SizeRequest;

class SizeController extends Controller
{
    public function index(){
        $sizes = Size::all();
        //dd($sizes);
        return view('dashboard.sizes.index' ,[
            'sizes' => $sizes
        ]);
    }

    public function create(){
        return view('dashboard.sizes.create');
    }

    public function store(SizeRequest $request){

        // $validator = Validator::make($request->all(), [
        //     'title' => 'required|unique:sizes|max:255|min:1',

        // ]);
 
        // if ($validator->fails()) {
        //     return redirect()->back()
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

    

        

        Size::create([

            'title' => $request->title

        ]);

        
return redirect()->route('sizes.index')->withMessage('Successfully Created!');
    }

    public function show($sizeId){

$size=Size::findOrFail($sizeId);


return view('dashboard.sizes.show',[
    'size' => $size
]);
//dd($size);
       // dd('showing'.$sizeId);
    }

    public function edit($sizeId){

$size=Size::findOrFail($sizeId);

return view('dashboard.sizes.edit',compact('size'));
    }





    public function update(SizeRequest $request,$sizeId){

    $size = Size::findOrFail($sizeId);

$size->update([
    'title' => $request->title
]);

return redirect()->route('sizes.index')->with('message','Successfully updated');


    }



    public function destroy($sizeId){
        $size = Size::findOrFail($sizeId);
       $size->delete();

       
       //return view('dashboard.categories.destroy');

       return redirect()->route('sizes.index')->withMessage('Successfully Deleted!');

    }

}
