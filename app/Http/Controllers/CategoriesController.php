<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use Illuminate\Contracts\Session\Session;
use App\Http\Requests\CategoryRequest;

class CategoriesController extends Controller
{
    public function index(){
     
       // $categories =Category::latest()->paginate(4);
        $categoryAll= Category::all();

        
        return view('dashboard.categories.index',[
            'categories'=>$categoryAll
        ]);

    }
    public function create(){
        return view('dashboard.categories.create');
    }

    public function store(CategoryRequest $request){
  
          if($file = $request->file('image')){
            $fileName=date('dmY').time().'.'.$file->getClientOriginalExtension();
            $file->move(storage_path('app/public/categories'),$fileName);

            
          }
          
        //validation

  

        //dd('raihan');

        Category::create([
                'title' => $request->title,
                'description' => $request->description,
                'is_active'=>$request->is_active,
                'image'=>$fileName ?? null
            ]);

            //Session::flash('message', 'Data Get Successfully!'); 
            
            // dd($request->all());

       return redirect()->route('categories.index')->with('message', 'Successfully Created!');


    }

    public function show($categoryId){

        $category=Category::findOrFail($categoryId);

        return view('dashboard.categories.show',compact('category'));

    }
    public function edit($categoryId){
        $category=Category::findOrFail($categoryId);
      //dd($category);

       return view('dashboard.categories.edit',compact('category'));

    }
    public function update(CategoryRequest $request,$categoryId){

        $category=Category::findOrFail($categoryId);

        if($file = $request->file('image')){
            $fileName=date('dmY').time().'.'.$file->getClientOriginalExtension();
            $file->move(storage_path('app/public/categories'),$fileName);

            
          }

        $category->update([

            'title' => $request->title,
            'description' => $request->description,
            'is_active'=>$request->is_active ?? false,
            'image'=>$fileName ?? $category->image


        ]);

        return redirect()->route('categories.index')->with('message', 'Successfully updated!');

        //return view();

//dd($request->all());
//dd($category);
    }
    public function destroy($categoryId){
        $category=Category::findOrFail($categoryId);
       $category->delete();

       //return view('dashboard.categories.destroy');

       return redirect()->route('categories.index')->with('message', 'Successfully Deleted!');

    }

}
