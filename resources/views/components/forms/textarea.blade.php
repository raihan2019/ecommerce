@props(['value','name'])
<x-forms.label for="{{$name}}Input" text="{{ucfirst($name)}}" />

<textarea name="{{$name}}" id="{{ $name.'Input'}}" {{$attributes->merge(['class'=>'form-control'])}}
    
> {{ $value }}</textarea>
<x-forms.error name="{{$name}}" />
