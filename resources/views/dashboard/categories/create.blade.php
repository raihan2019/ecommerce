<x-layout.master>
    <x-slot:pageTitle>
        Category Create
    </x-slot:pageTitle>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
        Category Create
            
            <a class="btn btn-sm btn-primary" href="{{ route('categories.index') }}">List</a>
        </div>
        <div class="card-body">


       <x-alert.errors />
 
     



 <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
              @csrf
               
            <div class="mb-3">

        <x-forms.input name="title" type="text" value="{{old('title')}}"/>
        <x-forms.input name="image" type="file" />
        <x-forms.textarea name="description" :value="old('description')" />



  


  <div class="mb-3">






    
  
  <div class="form-check">
<x-forms.checkbox />
</div>

<x-forms.button color="primary" class="mt-2" text="submit"/>                                
                                </form>


            </form>




            
        </div>
    </div>
</x-layout.master>