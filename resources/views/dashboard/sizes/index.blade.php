<x-layout.master>
    <x-slot:title>
        Size List
    </x-slot:title>
    <x-slot:pageTitle>
        Sizes
    </x-slot:pageTitle>
<div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                               Size List
                               <a class="btn btn-sm btn-primary" href="{{route('sizes.create')}}">Add New</a>
                            </div>
                          





                            <div class="card-body">
                           

                            @if(request()->session()->has('message'))
                            <div class="alert alert-success" role="alert">
                               Successfully Created
                            </div>

                            @endif


                                <table id="datatablesSimple">
                                    <thead>



                                        <tr>
                                            <th>Sl#</th>
                                            <th>Title</th>

                                            
                                        </tr>



                                    </thead>
                                    
                                    <tbody>
                                        @foreach($sizes as $size)

                                <tr>

                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $size->title }}</td>

                                    <td>
                                        <a href="{{ route('sizes.show',['size'=>$size->id]) }}" class="btn btn-sm btn-primary">Show</a>
                                        <a href="{{ route('sizes.edit',['size'=>$size->id]) }}" class="btn btn-sm btn-info">Edit</a>
                                       


                                <form method="post" action="{{ route('sizes.destroy',['size'=> $size->id]) }}" style="display:inline" >
                                    @csrf
                                    @method('delete')

                                    <button class="btn btn-sm btn-danger" onclick="return 
                                    confirm('Are you sure Want to Delete?')">
                                    Delete</button>                                
                                </form>




                                    </td>
                                    </tr>


                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @push('js')
                        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
                       <script src="{{asset('ui/backend')}}/js/datatables-simple-demo.js"></script>
                        @endpush
</x-layout.master>