<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Http\Requests\BrandRequest;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;


class BrandController extends Controller
{
    public function index(){
        $brands = Brand::all();
        //dd($sizes);
        return view('dashboard.brands.index' ,[
            'brands' => $brands
        ]);
    }

    public function create(){
        return view('dashboard.brands.create');
    }

    public function store(BrandRequest $request){

        // $validator = Validator::make($request->all(), [
        //     'title' => 'required|unique:sizes|max:255|min:1',

        // ]);
 
        // if ($validator->fails()) {
        //     return redirect()->back()
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }

    

        

        Brand::create([

            'title' => $request->title

        ]);

        
return redirect()->route('brands.index')->withMessage('Successfully Created!');
    }

    public function show($brandId){

$brand=Brand::findOrFail($brandId);


return view('dashboard.brands.show',[
    'brand' => $brand
]);
//dd($size);
       // dd('showing'.$sizeId);
    }

    public function edit($brandId){

$brand=Brand::findOrFail($brandId);

return view('dashboard.brands.edit',compact('brand'));
    }





    public function update(BrandRequest $request,$brandId){

    $brand = Brand::findOrFail($brandId);

$brand->update([
    'title' => $request->title
]);

return redirect()->route('brands.index')->with('message','Successfully updated');


    }



    public function destroy($brandId){
        $brand = Brand::findOrFail($brandId);
       $brand->delete();

       
       //return view('dashboard.categories.destroy');

       return redirect()->route('brands.index')->withMessage('Successfully Deleted!');

    }
}
