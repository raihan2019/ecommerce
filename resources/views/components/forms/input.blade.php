@props(['name'])

<x-forms.label for="{{$name}}Input" text="{{ucfirst($name)}}" />

<input name="{{ $name }}" id="{{$name.'Input'}}" 
 {{$attributes->merge([
    
    'class'=>"form-control"
    
    ])}}
>

<x-forms.error name="{{$name}}" />
