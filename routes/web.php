<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AutheController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\ShoppingController;
use App\Http\Controllers\SinglepageController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CourseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
        });
Route::prefix('admin')->group(function(){
    Route::get('/' ,[DashboardController::class,'home'])->name('admin.home');
    Route::get('/table' ,[DashboardController::class,'table']);
    Route::get('/products' ,[ProductController::class,'index'])->name('products.index');
    Route::get('/products/create' ,[ProductController::class,'create'])->name('products.create');
    Route::get('/products/delete' ,[ProductController::class,'delete'])->name('products.delete');
    Route::get('/products/edit' ,[ProductController::class,'edit'])->name('products.edit');
    Route::get('/products/update' ,[ProductController::class,'update'])->name('products.update');

   Route::get('/categories' ,[CategoriesController::class,'index'])->name('categories.index');
   Route::get('/categories/create' ,[CategoriesController::class,'create'])->name('categories.create');
   Route::post('/categories' ,[CategoriesController::class,'store'])->name('categories.store');
   Route::get('/categories/{category}' ,[CategoriesController::class,'show'])->name('categories.show');
   Route::get('/categories/{category}/edit' ,[CategoriesController::class,'edit'])->name('categories.edit');
   Route::patch('/categories/{category}/edit' ,[CategoriesController::class,'update'])->name('categories.update');
   Route::delete('/categories/{category}' ,[CategoriesController::class,'destroy'])->name('categories.destroy');

   Route::get('/sizes',[SizeController::class, 'index'])->name('sizes.index');
   Route::get('/sizes/create',[SizeController::class, 'create'])->name('sizes.create');
   Route::post('/sizes',[SizeController::class, 'store'])->name('sizes.store');
   Route::get('/sizes/{size}' ,[SizeController::class,'show'])->name('sizes.show');
   Route::get('/sizes/{size}/edit' ,[SizeController::class,'edit'])->name('sizes.edit');
   Route::patch('/sizes/{size}/edit' ,[SizeController::class,'update'])->name('sizes.update');
   Route::delete('/sizes/{size}' ,[SizeController::class,'destroy'])->name('sizes.destroy');
  

   Route::get('/tags',[TagController::class, 'index'])->name('tags.index');
   Route::get('/tags/create',[TagController::class, 'create'])->name('tags.create');
   Route::post('/tags',[TagController::class, 'store'])->name('tags.store');
   Route::get('/tags/{tag}' ,[TagController::class,'show'])->name('tags.show');
   Route::get('/tags/{tag}/edit' ,[TagController::class,'edit'])->name('tags.edit');
   Route::patch('/tags/{tag}/edit' ,[TagController::class,'update'])->name('tags.update');
   Route::delete('/tags/{tag}' ,[TagController::class,'destroy'])->name('tags.destroy');
   

   Route::get('/districts',[DistrictController::class, 'index'])->name('districts.index');
   Route::get('/districts/create',[DistrictController::class, 'create'])->name('districts.create');
   Route::post('/districts',[DistrictController::class, 'store'])->name('districts.store');
   Route::get('/districts/{district}' ,[DistrictController::class,'show'])->name('districts.show');
   Route::get('/districts/{district}/edit' ,[DistrictController::class,'edit'])->name('districts.edit');
   Route::patch('/districts/{district}/edit' ,[DistrictController::class,'update'])->name('districts.update');
   Route::delete('/districts/{district}' ,[DistrictController::class,'destroy'])->name('districts.destroy');


   Route::get('/brands',[BrandController::class, 'index'])->name('brands.index');
   Route::get('/brands/create',[BrandController::class, 'create'])->name('brands.create');
   Route::post('/brands',[BrandController::class, 'store'])->name('brands.store');
   Route::get('/brands/{brand}' ,[BrandController::class,'show'])->name('brands.show');
   Route::get('/brands/{brand}/edit' ,[BrandController::class,'edit'])->name('brands.edit');
   Route::patch('/brands/{brand}/edit' ,[BrandController::class,'update'])->name('brands.update');
   Route::delete('/brands/{brand}' ,[BrandController::class,'destroy'])->name('brands.destroy');

   Route::get('/courses',[CourseController::class, 'index'])->name('courses.index');
   Route::get('/courses/create',[CourseController::class, 'create'])->name('courses.create');
   Route::post('/courses',[CourseController::class, 'store'])->name('courses.store');
   Route::get('/courses/{course}' ,[CourseController::class,'show'])->name('courses.show');
   Route::get('/courses/{course}/edit' ,[CourseController::class,'edit'])->name('courses.edit');
   Route::patch('/courses/{course}/edit' ,[CourseController::class,'update'])->name('courses.update');
   Route::delete('/courses/{course}' ,[CourseController::class,'destroy'])->name('courses.destroy');




});

Route::prefix('frontend')->group(function(){
Route::get('/',[AutheController::class,'home'])->name('home');
Route::get('/categoryPage',[CategoryController::class,'category'])->name('category');
Route::get('/singlePage',[SinglepageController::class,'singlepage'])->name('singlepage');
Route::get('/shoppingcart',[ShoppingController::class,'shoppingcart'])->name('shoppingcart');
Route::get('/checkout',[CheckoutController::class,'checkout'])->name('checkout');
Route::get('/order',[OrderController::class,'order'])->name('order');
Route::get('/login',[LoginController::class,'login'])->name('login');
Route::get('/account',[AccountController::class,'account'])->name('account');
Route::get('/shop',[ShopController::class,'shop'])->name('shop');











});