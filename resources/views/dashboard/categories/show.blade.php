<x-layout.master>
    <x-slot:title>
        Category Details
    </x-slot:title>
    <x-slot:pageTitle>
        Category Details
    </x-slot:pageTitle>
<div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                               Category Details
                               <a class="btn btn-sm btn-primary" href="{{route('categories.index')}}">List</a>
                            </div>
                         
                            
                           





                            <div class="card-body">



                                <table class="table">
                                    
                                        <tr>
                                            <th>Title</th>
                                            <th>{{$category->title}}</th>
                                           
                                        </tr>
                                        <tr>
                                            <th>Image</th>
                                            <th><img src="{{ asset('storage/categories/'.$category->image) }}" alt="{{ $category->title }} Image" height="100"></th>
                                           
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <th>{{$category->description}}</th>
                                           
                                        </tr>
                                        <tr>
                                            <th>Is Active</th>
                                            <th>{{$category->is_active ? 'Active' : 'In Active'}}</th>
                                           
                                        </tr>
      
                                </table>
                            </div>
                        </div>
                        @push('js')
                        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
                       <script src="{{asset('ui/backend')}}/js/datatables-simple-demo.js"></script>
                        @endpush
</x-layout.master>